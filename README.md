Requirements
------------
* Xlib header files
* hack font

Instructions
------------

    git clone https://gitlab.com/watkinsr/dwm.git
    cd dwm && sudo make clean install


Patches/Features
----------------
* statuscolors
* statusallmons
* centredfloating
* savefloats
* notitle
* pertag2
* systray
* occupiedcol
* uselessgaps
* keysymfix
* bstack
* runorraise

New/Fixes
---
* re-wrote drawbar
* selected tag underline (configurable via config.h)
* tag spacing & padding (configurable via config.h)
* bstack horizonal gaps
